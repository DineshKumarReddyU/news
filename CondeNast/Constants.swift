//
//  Constants.swift
//  CondeNast
//
//  Created by Dinesh Reddy on 17/12/22.
//

import Foundation

struct Message {
    static let internetConnectionError = "Please check your connection and try reconnecting to internet"
    static let sessionExpired = "Your session has been expired, please login again"
}

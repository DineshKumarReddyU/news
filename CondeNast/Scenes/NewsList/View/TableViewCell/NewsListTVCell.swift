//
//  NewsListTVCell.swift
//  CondeNast
//
//  Created by Dinesh Reddy on 17/12/22.
//

import UIKit

class NewsListTVCell: UITableViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var newsImage: UIImageView!
    var tapBookmarkAction: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureUI(model: NewsArticles) {
        titleLabel.text = model.title ?? ""
        authorLabel.text = model.author ?? ""
        descriptionLabel.text = model.description ?? ""
        newsImage.loadImageUsingCacheWithURLString(model.urlToImage ?? "", placeHolder: nil) { (bool) in
//            cell.imageView.contentMode = .scaleAspectFit
        }
    }
    
    func configureLocalUI(model: NewsList) {
        titleLabel.text = model.title ?? ""
        authorLabel.text = model.author ?? ""
        descriptionLabel.text = model.newsDescription ?? ""
        newsImage.loadImageUsingCacheWithURLString(model.imageUrl ?? "", placeHolder: nil) { (bool) in
//            cell.imageView.contentMode = .scaleAspectFit
        }
    }
    
    @IBAction func bookmarkButtonAction(_ sender: Any) {
        self.tapBookmarkAction?()
    }
    
}

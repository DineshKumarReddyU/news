//
//  NewsDetailVC.swift
//  CondeNast
//
//  Created by Dinesh Reddy on 22/12/22.
//

import UIKit
import WebKit

class NewsDetailVC: UIViewController {
    @IBOutlet weak var newsWebView: WKWebView!
    var newsDetail : NewsArticles? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let request = URLRequest(url: URL(string: newsDetail?.url ?? "")!)
        self.newsWebView.load(request)
    }
}

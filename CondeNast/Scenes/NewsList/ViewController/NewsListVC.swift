//
//  ViewController.swift
//  CondeNast
//
//  Created by Dinesh Reddy on 17/12/22.
//

import UIKit

class NewsListVC: UIViewController {

    @IBOutlet weak var newsListTableView: UITableView!
    var viewModel = NewsListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        bind()
        self.viewModel.inputs.viewDidLoad()
    }
    
    func configureUI() {
        title = "News"
        newsListTableView.delegate = self
        newsListTableView.dataSource = self
        newsListTableView.register(UINib(nibName: "NewsListTVCell", bundle: nil), forCellReuseIdentifier: "NewsListTVCell")
    }
    
    func bind(){
        viewModel.reloadTableView = {[weak self] in
            guard let self = self else {return}
            DispatchQueue.main.async {
                self.newsListTableView.reloadData()
            }
        }
    }
    
    func booknmarkTapped(){
        print("Bookmark Tapped")
        /// No unique ID present from json data
    }
}

extension NewsListVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.error == .noInternet {
            return viewModel.outputs.getLocalStorageCount() ?? 0
        }
        return viewModel.outputs.getNewsListCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsListTVCell", for: indexPath) as? NewsListTVCell else {return UITableViewCell()}
        if viewModel.error == .noInternet {
            
            if let data = viewModel.outputs.getLocalStorageData(index: indexPath.row) {
                cell.configureLocalUI(model: data)
            }
        } else {
            if let data = viewModel.outputs.getNewsList(index: indexPath.row) {
                cell.configureUI(model: data)
            }
        }
        
        cell.tapBookmarkAction = {[weak self] in
            self?.booknmarkTapped()
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.error != .noInternet {
            let storyboard = UIStoryboard(name: "NewsList", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: "NewsDetailVC") as? NewsDetailVC else {return}
            if let data = viewModel.outputs.getNewsList(index: indexPath.row) {
                vc.newsDetail = data
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

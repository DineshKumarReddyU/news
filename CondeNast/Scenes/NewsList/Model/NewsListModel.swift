//
//  NewsListModel.swift
//  CondeNast
//
//  Created by Dinesh Reddy on 17/12/22.
//

import Foundation
struct NewsListModel : Codable {
    let status : String?
    let totalResults : Int?
    let articles : [NewsArticles]?
}
struct NewsArticles : Codable {
    let source : ArticleSource?
    let author : String?
    let title : String?
    let description : String?
    let url : String?
    let urlToImage : String?
    let publishedAt : String?
    let content : String?
}
struct ArticleSource : Codable {
    let name : String?
}

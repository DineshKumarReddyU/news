//
//  NewsListViewModel.swift
//  CondeNast
//
//  Created by Dinesh Reddy on 17/12/22.
//

import UIKit

protocol NewsProtocolInput {
    func viewDidLoad()
}

protocol NewsProtocolOutput : AnyObject {
    func getNewsList(index : Int) -> NewsArticles?
    func getNewsListCount() -> Int?
    func getLocalStorageCount() -> Int?
    func getLocalStorageData(index: Int) -> NewsList?
    var reloadTableView: (() -> Void) {get set}
}
protocol NewsProtocols {
    var inputs : NewsProtocolInput { get }
    var outputs : NewsProtocolOutput { get }
}

class NewsListViewModel: NSObject, NewsProtocolOutput, NewsProtocolInput, NewsProtocols {
    
    var inputs: NewsProtocolInput {return self}
    var outputs: NewsProtocolOutput {return self}
    var reloadTableView: (() -> Void) = {}
    
    var newsListData : NewsListModel? {
        didSet {
            reloadTableView()
        }
    }
    
    var error : NetworkError?
    
    var localNewsList : [NewsList]? {
        didSet {
            reloadTableView()
        }
    }
    
    func viewDidLoad() {
        getNewsListApi()
    }
    
    func getNewsListApi() {
        Webservice.shared.getWeatherByCity { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .success(let news) :
                let decoder = JSONDecoder()
                guard let json: NewsListModel = try? decoder.decode(NewsListModel.self, from: news) else {return}
                self.newsListData = json
                CoreDataManager.shared.deleteAllNewsData() /// Empty coredata entity before adding new data
                if let news = self.newsListData?.articles {
                    for item in news {
                        CoreDataManager.shared.saveNewsInfo(item) /// Save new data to coreData
                    }
                }
            case .failure(let err) :
                self.error = err
                self.fetchCoreDataResults() /// Load local storage if error
                print("err = \(err)")
            }
        }
    }
    
    func getNewsList(index : Int) -> NewsArticles? {
        return newsListData?.articles?[index]
    }
    
    func getNewsListCount() -> Int? {
         return newsListData?.articles?.count ?? 0
    }
    
    func fetchCoreDataResults(){
        localNewsList = CoreDataManager.shared.fetchEntity(NewsList.self) as? [NewsList]
    }
    
    func getLocalStorageCount() -> Int? {
        return localNewsList?.count ?? 0
    }
    
    func getLocalStorageData(index: Int) -> NewsList? {
        return localNewsList?[index]
    }
}

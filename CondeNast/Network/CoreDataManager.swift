
import UIKit
import CoreData


class CoreDataManager {
    //1
    static let shared = CoreDataManager()
    //2
    let managedContext = appDelegate.persistentContainer.viewContext
    //3
    func fetchEntity(_ classObject: AnyClass) -> [Any] {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: NSStringFromClass(classObject.self))
        var result: [Any]? = nil
        do {
            result = try managedContext.fetch(fetchRequest)
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return result!
    }
    //4
    func saveEntity() {
        do {
            try managedContext.save()
            print("✅ saved succesfuly")
        } catch let error as NSError {
            print("❌ Failed to create : \(error.localizedDescription)")
        }
    }
    
    func saveNewsInfo(_ model: NewsArticles) {
        let information = NSEntityDescription.insertNewObject(forEntityName: NSStringFromClass(NewsList.self), into: managedContext) as! NewsList
        information.title = model.title
        information.imageUrl = model.urlToImage
        information.newsDescription = model.description
        information.author = model.author
        information.newsUrl = model.url
        //
        saveEntity()
    }
    func deleteAllNewsData(){
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "NewsList")

        // Create Batch Delete Request
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

        do {
            try managedContext.execute(batchDeleteRequest)

        } catch {
            // Error Handling
        }
    }
}

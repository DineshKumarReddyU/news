//
//  NetworkManager.swift
//  CondeNast
//
//  Created by Dinesh Reddy on 17/12/22.
//
import Foundation
    
enum NetworkError: Error {
    case badURL
    case noData
    case decodingError
    case noInternet
}


class Webservice {
    static let shared = Webservice()
    func getWeatherByCity(completion: @escaping((Result<Data, NetworkError>) -> Void)) {
        
        guard let newsURL = URL(string: "https://newsapi.org/v2/everything?q=tesla&from=2022-11-22&sortBy=publishedAt&apiKey=52439d6ea1da4263973ccdf3f728f23b") else {return}
        
        URLSession.shared.dataTask(with: newsURL) { data, response, error in
            
            guard let data = data, error == nil else {
                if error?.localizedDescription == "The Internet connection appears to be offline." {
                    return completion(.failure(.noInternet))
                }
                return completion(.failure(.noData))
            }
            
            completion(.success(data))
        }.resume()
        
    }
}
